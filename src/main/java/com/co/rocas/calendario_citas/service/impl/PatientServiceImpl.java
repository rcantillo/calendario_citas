package com.co.rocas.calendario_citas.service.impl;

import com.co.rocas.calendario_citas.domain.dto.PatientDto;
import com.co.rocas.calendario_citas.domain.mapper.PatientMapper;
import com.co.rocas.calendario_citas.domain.model.Patient;
import com.co.rocas.calendario_citas.domain.repository.PatientRepository;
import com.co.rocas.calendario_citas.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PatientServiceImpl implements PatientService {

    @Autowired
    private PatientRepository patientRepository;


    @Override
    public PatientDto findById(Integer id) {
        Patient patient = patientRepository.findById(id).orElseThrow(() ->
                new NullPointerException("NOT FOUND PACIENTE")
        );
        return PatientMapper.INSTANCE.patientToDto(patient);
    }

    @Override
    public PatientDto create(PatientDto patient) {
        return PatientMapper.INSTANCE.patientToDto(patientRepository.save(PatientMapper.INSTANCE.dtoToPatient(patient)));
    }
}
