package com.co.rocas.calendario_citas.service;

import com.co.rocas.calendario_citas.domain.dto.PatientDto;

public interface PatientService {

    PatientDto findById(Integer id);
    PatientDto create(PatientDto patient);
}
