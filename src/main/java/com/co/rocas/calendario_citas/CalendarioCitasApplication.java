package com.co.rocas.calendario_citas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalendarioCitasApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalendarioCitasApplication.class, args);
	}

}
