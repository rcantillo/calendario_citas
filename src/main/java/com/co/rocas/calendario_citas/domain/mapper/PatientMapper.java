package com.co.rocas.calendario_citas.domain.mapper;

import com.co.rocas.calendario_citas.domain.dto.PatientDto;
import com.co.rocas.calendario_citas.domain.model.Patient;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PatientMapper {

    PatientMapper INSTANCE = Mappers.getMapper(PatientMapper.class);

    PatientDto patientToDto (Patient patient);
    Patient dtoToPatient (PatientDto patient);
}
