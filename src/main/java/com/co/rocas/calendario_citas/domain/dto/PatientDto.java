package com.co.rocas.calendario_citas.domain.dto;

import com.co.rocas.calendario_citas.domain.model.Address;
import com.co.rocas.calendario_citas.domain.utils.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PatientDto extends BaseEntity {

    private String patientName;
    private String patientIdNumber;
    private String patientPhone;
    private List<Address> addresses;




}
