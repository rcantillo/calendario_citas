package com.co.rocas.calendario_citas.domain.model;

import com.co.rocas.calendario_citas.domain.utils.BaseEntity;
import lombok.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "address")
public class Address extends BaseEntity {
    private String neighborhood;
    private String country;
    private String location;

}
