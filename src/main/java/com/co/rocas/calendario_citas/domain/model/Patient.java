package com.co.rocas.calendario_citas.domain.model;

import com.co.rocas.calendario_citas.domain.utils.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "patient")
public class Patient extends BaseEntity {

    private String patientName;
    @Column(name="id_number", nullable = false, unique = true)
    private String patientIdNumber;
    private String patientPhone;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "patient_id", referencedColumnName = "id")
    private List<Address> addresses;

}
