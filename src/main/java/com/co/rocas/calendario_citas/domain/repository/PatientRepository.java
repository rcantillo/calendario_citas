package com.co.rocas.calendario_citas.domain.repository;

import com.co.rocas.calendario_citas.domain.model.Patient;
import org.springframework.data.repository.CrudRepository;


public interface PatientRepository extends CrudRepository<Patient, Integer> {
}
