package com.co.rocas.calendario_citas.interfaces;

import com.co.rocas.calendario_citas.domain.dto.PatientDto;
import com.co.rocas.calendario_citas.domain.mapper.PatientMapper;
import com.co.rocas.calendario_citas.domain.model.Patient;
import com.co.rocas.calendario_citas.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/patient")
public class PatientController {

    @Autowired
    private PatientService patientService;

    @GetMapping("/{patient_id}")
    public ResponseEntity<PatientDto> findPatient(@PathVariable Integer patient_id){
        return new ResponseEntity<>(patientService.findById(patient_id) , HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<Patient> create(@RequestBody PatientDto patient){
        PatientDto patientDto = patient;
        Patient patientSave = PatientMapper.INSTANCE.dtoToPatient(patientService.create(patientDto));
        
        return new ResponseEntity<>(patientSave, HttpStatus.CREATED);
    }
}
